const quickSort = require("./quick-sort");

describe("Quick Sort", () => {
  test("Level 1", () => {
    const input = [4, 2, 3, 1];
    const output = [1, 2, 3, 4];

    expect(quickSort(input)).toEqual(output);
  });
});
