class Aray {
  constructor() {
    this.data = "";
  }

  push(value) {
    this.data += value;
  }

  pop() {
    let value = this.data[this.data.length - 1];
    let data = "";

    for (let i = 0; i < this.data.length - 1; i++) {
      data += this.data[i];
    }

    this.data = data;

    return value;
  }

  length() {
    return this.data.length;
  }
}

module.exports = Aray;
