class Queue {
  constructor() {
    this.data = {};
    this.front = 0;
    this.back = 0;
  }

  enqueue(value) {
    this.data[this.front++] = value;
  }

  dequeue() {
    if (this.front <= this.back) {
      return null;
    }

    return this.data[this.back++];
  }

  peek() {
    return this.data[this.back];
  }
}

module.exports = Queue;
