const bubbleSort = require("./bubble-sort");

describe("Bubble Sort", () => {
  test("Simple", () => {
    const input = [3, 2, 1];
    const output = [1, 2, 3];
    expect(bubbleSort(input)).toEqual(output);
  });

  test("Advance", () => {
    const input = [10, 5, 3, 8, 2, 6, 4, 7, 9, 1];
    const output = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    expect(bubbleSort(input)).toEqual(output);
  });
});
