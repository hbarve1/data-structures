const insertionSort = require("./insertion-sort");

describe("Insertion Sort", () => {
  test("Level 1", () => {
    const input = [1, 3, 2];
    const output = [1, 2, 3];

    expect(insertionSort(input)).toEqual(output);
  });

  test("Level 2", () => {
    const input = [5, 3, 2, 4, 1];
    const output = [1, 2, 3, 4, 5];

    expect(insertionSort(input)).toEqual(output);
  });

  test("Level 3", () => {
    const input = [10, 5, 3, 8, 2, 6, 4, 7, 9, 1];
    const output = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    expect(insertionSort(input)).toEqual(output);
  });
});
