let print = (msg = "--------") => console.log(`\n${msg}`);

function basicRecursion(max, current) {
  if (current > max) return;
  print(current);
  basicRecursion(max, current + 1);
}

basicRecursion(5, 1);
