const Aray = require("./Array");

describe("Array", () => {
  let array;
  beforeEach(() => {
    array = new Aray();
  });

  test("Constructor", () => {
    expect(array).toBeInstanceOf(Aray);

    expect(array).toHaveProperty("push");
    expect(array).toHaveProperty("pop");
    expect(array).toHaveProperty("length");
  });

  test("Length", () => {
    expect(array.length()).toBe(0);
  });

  test("Push", () => {
    array.push("a");
    array.push("b");
    array.push("c");
    array.push("d");
    array.push("e");
    array.push("f");

    expect(array.length()).toBe(6);
  });

  test("Pop", () => {
    array.push("a");
    array.push("b");
    array.push("c");
    array.push("d");
    array.push("e");
    array.push("f");

    expect(array.length()).toBe(6);
    expect(array.pop()).toBe("f");
    expect(array.length()).toBe(5);
  });
});
