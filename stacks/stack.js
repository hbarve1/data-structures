class Stack {
  constructor() {
    this.data = {};
    this.length = 0;
  }

  push(value) {
    this.data[this.length++] = value;
  }

  pop() {
    const value = this.data[this.length - 1];
    delete this.data[this.length - 1];

    this.length--;

    return value;
  }

  peek() {
    return this.data[this.length - 1];
  }

  getData() {
    return Object.entries(this.data);
  }
}

module.exports = Stack;
