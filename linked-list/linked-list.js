/*
  LinkedList

  Name your class / constructor (something you can call new on) LinkedList

  LinkedList is made by making nodes that have two properties, the value that's being stored and a pointer to
  the next node in the list. The LinkedList then keep track of the head and usually the tail (I would suggest
  keeping track of the tail because it makes pop really easy.) As you may have notice, the unit tests are the
  same as the ArrayList; the interface of the two are exactly the same and should make no difference to the
  consumer of the data structure.

  I would suggest making a second class, a Node class. However that's up to you how you implement it. A Node
  has two properties, value and next.

  length - integer  - How many elements in the list
  push   - function - accepts a value and adds to the end of the list
  pop    - function - removes the last value in the list and returns it
  get    - function - accepts an index and returns the value at that position
  delete - function - accepts an index, removes value from list, collapses,
                      and returns removed value

  As always, you can change describe to xdescribe to prevent the unit tests from running while
  you work
*/

class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.length = 0;
    this.head = null;
    this.tail = null;
  }

  test(search, nodeValue) {
    return search === nodeValue;
  }

  testIndex(search, __, i) {
    return search === i;
  }

  serialize() {
    const ans = [];
    let current = this.head;

    if (!current) return ans;

    while (current) {
      ans.push(current.value);
      current = current.next;
    }

    return ans;
  }

  _find(value, testFn = this.test) {
    let current = this.head;
    let i = 0;

    while (current) {
      if (testFn(value, current.value, i, current)) {
        return current;
      }

      current = current.next;
      i++;
    }

    return null;
  }

  push(value) {
    const node = new Node(value);
    this.length++;

    if (!this.head) {
      this.head = node;
    } else {
      this.tail.next = node;
    }

    this.tail = node;
  }

  pop() {
    if (!this.head) return null;

    if (this.head === this.tail) {
      const node = this.head;
      this.head = this.tail = null;

      return node.value;
    }

    const secondLast = this._find(null, (value, nodeValue, i, current) => {
      return current.next === this.tail;
    });

    const ans = secondLast.next.value;
    secondLast.next = null;
    this.tail = secondLast;
    this.length--;

    return ans;
  }

  get(index) {
    if (!this.head) return null;
    if (this.length < index) return null;

    let pointer = this.head;

    for (let i = 0; i < index; i++) {
      pointer = pointer.next;
    }

    return pointer.value;
  }

  delete(index) {
    if (index === 0) {
      const head = this.head;

      if (head) {
        this.head = head.next;
      } else {
        this.head = this.tail = null;
      }

      this.length--;
      return head.value;
    }

    const previousNode = this._find(index - 1, this.testIndex);

    const current = previousNode.next;

    if (!current) return null;

    previousNode.next = current.next;

    if (!previousNode.next.next) this.tail = previousNode.next;

    this.length--;
    return current.value;
  }
}

module.exports = LinkedList;
